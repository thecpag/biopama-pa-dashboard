jQuery(document).ready(function($) {
  var allResults = {};
  var ACPStatsURL = "https://api.biopama.org/api/protection_level/function/api_acp_stats/";
  var RegionStatsURL = "https://api.biopama.org/api/protection_level/function/api_region_stats/";
  var CountryListURL = "https://api.biopama.org/api/protection_level/function/api_country_list_stats/region=";
  var RegionPACatsURL = "https://api.biopama.org/api/protection_level/function/api_region_iucn_cat/region_acp=";
  var regionGovTypeURL = "https://api.biopama.org/api/protection_level/function/api_region_gov_type/region_acp=";
  var regionDesigURL = "https://api.biopama.org/api/protection_level/function/api_region_desig/region_acp=";
  var tableAttribution = $("#dashboard-table-citation").text();
  var fullAttribution = document.getElementById('dashboard-attribution');
  var cloneFullAttribution = fullAttribution.cloneNode(true);
  $('#pa-dashboard-description').append(cloneFullAttribution);
  var paDashboardLegend = {
      textStyle: {
        color: "rgb(128, 128, 128)",
      },
      show: true,
      bottom: '0%',
  };
  var paDashboardTooltip = {
    show: true,
    trigger: 'item',
    formatter: "{d}%"
  };
  var paDashboardTooltipWithNumber = {
    show: true,
    trigger: 'item',
    formatter: "{c} ({d}%)"
  };

  $.each(biopamaGlobal.regions,function(idx,region){
    var thisRegion = {};
    if (region.id !== 'ACP'){
      emptyCard(region);
      $.when(
        $.getJSON(CountryListURL+region.id,function(d){
          allResults[region.id] = d;
        }),
        $.getJSON(RegionPACatsURL+region.id,function(d){
          allResults[region.id+"cats"] = d;
        }),
      ).then(function() {
        thisRegion = getRegionStats(region.id)
        buildDashboardCharts(thisRegion);
        buildDashboardTable(region.id);
        updateColors();
      });
    } else {
      $.when(
        $.getJSON(ACPStatsURL,function(d){
          allResults.acp = d[0];
        }),
        $.getJSON(RegionStatsURL,function(d){
          allResults.regions = d;
        }),
      ).then(function() {
        thisRegion = allResults.acp;
        buildDashboardCharts(thisRegion);
        updateColors();
      });
    }
  });

  function updateColors(){
    $(".color-ter").css("color", biopamaPAColors.terrestrial);
    $(".color-mar").css("color", biopamaPAColors.marine);
    $(".color-cos").css("color", biopamaPAColors.coastal);
    $(".color-all").css("color", biopamaPAColors.totalArea);
  }

  function getRegionStats(regionID){
    var regionArray = allResults.regions;
    var thisRegion =regionArray.find(obj => {
      return obj.region === regionID
    });
    return thisRegion;
  }
  function buildDashboardCharts(region){
    if (!region.region){
      region.region = 'ACP';
    }
    var terAreaProtTotal = Number(region.ter_area_prot / 1000).toFixed(2);
    var marAreaProtTotal = Number(region.mar_area_prot / 1000).toFixed(2);
    var areaProtTotal = Number(terAreaProtTotal) + Number(marAreaProtTotal);

    $( 'div#' + region.region + '-area-pa-total' ).text(areaProtTotal.toFixed(2));
    $( 'div#' + region.region + '-area-ter-pa-total' ).text(terAreaProtTotal);
    $( 'div#' + region.region + '-area-mar-pa-total' ).text(marAreaProtTotal);
    $( 'div#' + region.region + '-num-ter-pa-total' ).text(region.ter_count);
    $( 'div#' + region.region + '-num-mar-pa-total' ).text(region.mar_count);
    $( 'div#' + region.region + '-num-cos-pa-total' ).text(region.cos_count);
    /*
    ## Start Region Area Protected Terrestrial Area  ##
    */
    var chartValues = calcChartValues(region.ter_perc_prot);
    var chartData = {
      colors: "twoTerrestrial",
      legend: paDashboardLegend,
      tooltip: paDashboardTooltip,
      series: [{
        type: 'pie',
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: [
          {value: chartValues[0], name: 'Terrestrial Protection'},
          {value: chartValues[1], name: 'Not Protected'},
        ]
      }]
    };
    $().createNoAxisChart('#' + region.region + '-pie-chart-t', chartData);
    $( '#' + region.region + '-pie-chart-value-t' ).text(chartValues[0]+'%');
    /*
    ## End Region Area Protected Terrestrial Area  ##
    */
    /*
    ## Start Region Area Protected Marine Area  ##
    */
    var chartValues = calcChartValues(region.mar_perc_prot)
    var chartData = {
      colors: "twoMarine",
      legend: paDashboardLegend,
      tooltip: paDashboardTooltip,
      series: [{
        type: 'pie',
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: [
          {value: chartValues[0], name: 'Marine Protection'},
          {value: chartValues[1], name: 'Not Protected'},
        ]
      }]
    }
    $().createNoAxisChart('#' + region.region + '-pie-chart-m', chartData);
    $( '#' + region.region + '-pie-chart-value-m' ).text(chartValues[0]+'%');
    /*
    ## End Region Area Protected Marine Area  ##
    */
  };
  function calcChartValues(chartValue){
    var chartValues = [];
    chartValue = Number(chartValue).toFixed(2);
    chartValues.push(chartValue);
    var missingValue = 100 - chartValue;
    chartValues.push(missingValue);
    return chartValues;
  }
  function buildDashboardTable(regionID){
    const dataSet = allResults[regionID];
    dataSet.forEach((item) => {
      // Iterate over the properties in each object
      Object.keys(item).forEach((key) => {
        // Check if the key contains the letters 'area'
        if (key.includes("area")) {
          // Check if the value can be converted to a number
          if (!isNaN(parseFloat(item[key]))) {
            // Convert the value to have one decimal place
            item[key] = parseFloat(item[key]).toFixed(1);
          }
        }
      });
    });
    //number of terrestrial PAs, number of marine PAs, number of coastal PAs, terrestrial protection (km2), marine protection (km2), terrestrial protection (%), marine protection (%)
    var columnData = [
      {
        title: "ISO2",
        data: "iso2",
        "visible": false,
        "searchable": true
      },{
        title: "Country",
        data: "name",
        //responsivePriority: 0
      },{
        title: "Country Area",
        data: "ter_area",
        //responsivePriority: 0
      },{
        title: "Total PAs",
        data: "total_count",
        //responsivePriority: 1
      },{
        title: "Terrestrial PAs",
        data: "ter_count",
        //responsivePriority: 2,
        //className: "min-desktop",
      },{
        title: "Marine PAs",
        data: "mar_count",
        //responsivePriority: 2
        //className: "min-desktop",
      },{
        title: "Coastal PAs",
        data: "cos_count",
        //responsivePriority: 2
        //className: "min-desktop",
      },
      {
        title: "Terrestrial Protection (%)",
        data: "ter_perc_prot",
        //responsivePriority: 0
      },
      {
        title: "Terrestrial Protection (km2)",
        data: "ter_area_prot",
        //responsivePriority: 0
      },
      {
        title: "Marine Protection (%)",
        data: "mar_perc_prot",
        //responsivePriority: 0
      },
      {
        title: "Marine Protection (km2)",
        data: "mar_area_prot",
        //responsivePriority: 0
      },
    ];
    var columnSettings = [{ "className": regionID + "-cell region-cell", "targets": "_all" },
    {
      "targets": [ 1 ],
      "createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
        $(cell).html('<a class"dashboard-country-link" href="https://tools.thecpag.org/ct/country/'+rowData.iso2+'">'+rowData.name+'</a>');
      }
    }];
    var tableData = {
      columns: columnData,
      columnDefs: columnSettings,
      data: dataSet,
      attribution: tableAttribution,
      isComplex: true,
      pagination: false,
      pageLength: 20,
      drawCallback: function () {
        var thisRegion = getRegionStats(regionID);
        // console.log(thisRegion);
        var api = this.api();
        var footer = '<td>' + api.rows().count() + ' countries</td>' +
        '<td>' + (parseFloat(thisRegion.ter_area).toFixed(2) / 1000).toFixed(2) + ' / 1000km<sup>2</sup></td>'+ //Area of Country
        '<td>' + thisRegion.total_count + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.ter_count + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.mar_count + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.cos_count + ' PAs</td>'+ //# of PAs
          '<td>' + parseFloat(thisRegion.ter_perc_prot).toFixed(2) + ' %</td>'+ //% Terrestrial Protected
          '<td>' + (parseFloat(thisRegion.ter_area_prot).toFixed(2) / 1000).toFixed(2) + ' / 1000km<sup>2</sup></td>'+ //Area Terrestrial Protected
          '<td>' + parseFloat(thisRegion.mar_perc_prot).toFixed(2) + ' %</td>' + //% Marine Protected
          '<td>' + (parseFloat(thisRegion.mar_area_prot).toFixed(2) / 1000).toFixed(2) + ' / 1000km<sup>2</sup></td>'; //Area Marine Protected
        $( api.table().footer() ).html(footer);
      }
    }
    $().createDataTable(regionID, tableData);
  }
  function emptyCard(region){
    var regionHeader = '<h1>' + region.name + ' Protected Areas Coverage</h1>';
    var regionTable = '<div class="card-body">'+
      '<table id="' + region.id + '" class="table biopama-table" style="width:100%"><thead><tr></tr></thead><tbody></tbody><tfoot><tr></tr></tfoot></table>'+
    '</div>';
    const card = document.getElementById('region-protected-area-stats-card');
    const newCard = card.cloneNode(true);
    newCard.id = region.id +'card';
    // hidden
    $(newCard).find(".region-header").html(regionHeader);
    $(newCard).find("#ACP-accordion").removeClass("hidden");
    $(newCard).find(".region-table").html(regionTable);
    $(newCard).find(".region-id-change").each(function( index ) {
      var thisID = $( this ).attr("id");
      var newID = thisID.substring(3);
      newID = region.id + newID;
      this.id = newID;
    });
    $(newCard).find(".region-target-change").each(function( index ) {
      var thisID = $( this ).attr("id");
      var newID = thisID.substring(7);
      newID = region.id + newID;
      $( this ).attr("data-bs-target",'#'+newID);
      $( this ).attr("aria-controls",newID);
    });
    $('#pa-dashboard-cards').replaceWith(newCard);
    $('#'+region.id+'-collapse-IUCN-Cats').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(RegionPACatsURL+region.id,function(d){
            regionData = d;
          })
        ).then(function() {
          buildCatPieChart(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    $('#'+region.id+'-collapse-governance-types').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(regionGovTypeURL+region.id,function(d){
            regionData = d;
          })
        ).then(function() {
          buildGovPieChart(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    $('#'+region.id+'-collapse-designations').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(regionDesigURL+region.id,function(d){
            regionData = d;
          })
        ).then(function() {
          buildDesigTable(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    return;
  }
  function buildCatPieChart(regionID, regionData){
    // console.log(regionData)
    //assign order to the results
    $.each(regionData,function(idx,cat){
      switch (cat.iucn_cat) {
        case "Ia":
          cat.order = 1;
          break;
        case "Ib":
          cat.order = 2;
          break;
        case "II":
          cat.order = 3;
          break;
        case "III":
          cat.order = 4;
          break;
        case "IV":
          cat.order = 5;
          break;
        case "V":
          cat.order = 6;
          break;
        case "VI":
          cat.order = 7;
          break;
        case "Not Assigned":
          cat.order = 8;
          break;
        case "Not Applicable":
          cat.order = 9;
          break;
        case "Not Reported":
          cat.order = 10;
          break;
      };
    });
    var sortedRegionCats = regionData.sort( $().sortObject("order", "asc") );
    var chartSeriesData = [];
    $.each(sortedRegionCats,function(idx,obj){
      var thisObj = {};
      thisObj.value = obj.sum;
      thisObj.name = obj.iucn_cat;
      chartSeriesData.push(thisObj);
    });
    var chartData = {
      legend: paDashboardLegend,
      tooltip: paDashboardTooltipWithNumber,
      series: [{
        type: 'pie',
        bottom: '25%' ,
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: chartSeriesData
      }]
    };
    $().createNoAxisChart('#' + regionID + '-pie-chart-cat', chartData);
  }
  function buildGovPieChart(regionID, regionData){
    var sortedRegionData = regionData.sort( $().sortObject("gov_type", "asc") );
    var chartSeriesData = [];
    $.each(sortedRegionData,function(idx,obj){
      var thisObj = {};
      thisObj.value = obj.sum;
      thisObj.name = obj.gov_type;
      chartSeriesData.push(thisObj);
    });
    var chartData = {
      legend: paDashboardLegend,
      tooltip: paDashboardTooltipWithNumber,
      series: [{
        type: 'pie',
        bottom: '25%' ,
        radius: ['45%', '50%'],
        avoidLabelOverlap: true,
        data: chartSeriesData
      }]
    };
    $().createNoAxisChart('#' + regionID + '-pie-chart-gov', chartData);
  }
  function buildDesigTable(regionID, regionData){
    var dataSet = regionData;
    //number of terrestrial PAs, number of marine PAs, number of coastal PAs, terrestrial protection (km2), marine protection (km2), terrestrial protection (%), marine protection (%)
    var columnData = [
      {
        title: "Designation",
        data: "desig_eng",
      },{
        title: "Total",
        data: "sum",
      }
    ];
    var columnSettings = [{ "className": regionID + "-cell region-cell", "targets": "_all" }];
    var tableData = {
      columns: columnData,
      columnDefs: columnSettings,
      data: dataSet,
      attribution: tableAttribution,
      isComplex: true,
      pagination: true,
    }
    $().createDataTable(regionID+"-table-desig", tableData);
  }
});
