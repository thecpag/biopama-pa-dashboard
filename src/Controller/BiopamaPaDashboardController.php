<?php
/**
 * @file
 * Contains \Drupal\biopama_pa_dashboard\Controller\BiopamaPaDashboardController.
 */
 
namespace Drupal\biopama_pa_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines Protected Area Dashboard class.
 */
class BiopamaPaDashboardController extends ControllerBase {
  public function dashboard() {
    $element = array(
        '#theme' => 'pa_dashboard',
      );
      return $element;
  }
}